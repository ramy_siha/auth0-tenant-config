import { deploy, dump } from 'auth0-deploy-cli';

const config = {
  AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
  AUTH0_CLIENT_SECRET: process.env.AUTH0_CLIENT_SECRET,
  AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
  AUTH0_ALLOW_DELETE: false
};

// Import tenant config
deploy({
  input_file: process.env.INPUT_FILE,  // Input file for directory, change to .yaml for YAML
  config_file: process.env.CONFIG_FILE                  // Option to a config json
})
  .then(() => console.log('yey deploy was successful'))
  .catch(err => console.log('Oh no, something went wrong. Error: ${err}'));
